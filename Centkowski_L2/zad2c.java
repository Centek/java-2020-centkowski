import java.util.Scanner;
import java.util.Random;

public class Lab2_z2c
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe od 1 do 100: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        
        generuj(tablica, n, -999, 999);
        
	System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        System.out.println("");
        
        System.out.println("Maksymalna liczba wystepuje: " + ile_Maksymalnych(tablica) + " razy");
    }

    public static void generuj (int[] tab, int n, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max - min + 1) + min;
        }
    }

    public static int ile_Maksymalnych(int tab[]) {
        int max = tab[0]; 

        for (int el : tab) {
            if (el > max) {
                max = el;
            }
        }
        
        int ile = 0;
        
        for (int el : tab) {
            if (el == max) {
                ++ile;
            }
        }

        return ile;
    }
}

