import java.util.Scanner;
import java.util.Random;

public class Lab2_z1f
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj ilosc liczb: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        int lim1 = 999;
        int lim2 = -999;
        
        Random r = new Random();
        
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(lim1 - lim2 + 1) + lim2;
        }
        
        System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        System.out.println();
        
        for (int j = 0; j < n; ++j) {
            if (tablica[j] > 0) {
                tablica[j] = 1;
            } else if (tablica[j] < 0) {
                tablica[j] = -1;
            }
        }
        
        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }

        System.out.println();
    }
}
