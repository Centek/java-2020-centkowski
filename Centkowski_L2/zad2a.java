import java.util.Scanner;
import java.util.Random;

public class Lab2_z2a
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe od 1 do 100: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        
	System.out.println();

        generuj(tablica, n, -999, 999);
        
        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        System.out.println("");        
        
        System.out.println("Liczby nieparzyste: " + ile_Nieparzystych(tablica));
        System.out.println("Liczby parzyste: " + ile_Parzystych(tablica));
    }

    public static void generuj (int[] tab, int n, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max - min + 1) + min;
        }
    }

    public static int ile_Nieparzystych(int tab[]) {
        int ile_niep = 0;
            
        for (int el : tab) {
            if (Math.abs(el) %2 == 1) {
                ile_niep++;
            }
        }

        return ile_niep;
    }

    public static int ile_Parzystych(int tab[]) {
        int ile_parz = 0;
            
        for (int el : tab) {
            if (Math.abs(el) %2 == 0) {
                ile_parz++;
            }
        }

        return ile_parz;
    }
}
