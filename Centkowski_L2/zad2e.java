import java.util.Scanner;
import java.util.Random;

public class Lab2_z2e
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj ilosc liczb: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        
        generuj(tablica, n, -999, 999);
        
	System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        
        
        System.out.println("\n Maksymalna dlugosc ciagu liczb dodatnich: " + dlugoscMaxCiaguDodatnich(tablica));
    }

    public static void generuj (int[] tab, int n, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max - min + 1) + min;
        }
    }

    public static int dlugoscMaxCiaguDodatnich(int tab[]) {
        int dlugosc = 0;
        int max_dlugosc = 0;
        
        for (int el : tab) {
            if (el > 0) {
                ++dlugosc;
                if (dlugosc > max_dlugosc) {
                    max_dlugosc = dlugosc;
                }
            } else {
                dlugosc = 0;
            }
        }

        return max_dlugosc;
    }
}

