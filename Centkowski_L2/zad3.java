import java.util.Scanner;
import java.util.Random;

public class Lab2_z3
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int m = 0;
        
        while (!((m >= 1) && (m <= 10))) {
            System.out.println("Podaj liczbe m <1; 10>: ");
            m = in.nextInt();
        }

        int n = 0;
        
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe n <1; 10>: ");
            n = in.nextInt();
        }

        int k = 0;
        
        while (!((k >= 1) && (k <= 100))) {
            System.out.println("Podaj liczbe k <1; 10>: ");
            k = in.nextInt();
        }
        
        int[][] tab_a = new int[m][n];
        generuj(tab_a, m, n, -10, 10);
        
        int[][] tab_b = new int[n][k];
        generuj(tab_b, n, k, -10, 10);

        System.out.println();
        System.out.printf("\nMacierz a:\n\n");

        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                System.out.printf("%5d", tab_a[i][j]);
            }
            System.out.println();
        }
        
        System.out.println();
        System.out.printf("\n Macierz b:\n\n");

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < k; ++j) {
                System.out.printf("%5d", tab_b[i][j]);
            }
            System.out.println();
        }
        
        System.out.println();
        System.out.printf("\n Macierz c (a x b):\n\n");
        int[][] tab_c = new int[m][k];


        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < k; ++j) {
                for (int h = 0; h < n; ++h) {
                    tab_c[i][j] += tab_a[i][h] * tab_b[h][j];
                }
            }
        }

        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < k; ++j) {
                System.out.printf("%5d", tab_c[i][j]);
            }
            System.out.println();
        }

        System.out.println();
    }

    public static void generuj (int[][] tab, int m, int n, int min, int max) {
        Random r = new Random();
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                tab[i][j] = r.nextInt(max - min + 1) + min;
            }
        }
    }
}




