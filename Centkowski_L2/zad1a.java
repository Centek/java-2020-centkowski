import java.util.Scanner;
import java.util.Random;

public class Lab2_z1a
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe od 1 do 100: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        
        int lim1 = 999;
        int lim2 = -999;
        
	System.out.println();

        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(lim1 - lim2 + 1) + lim2;
        }
        
        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
		
        System.out.println("");
        
		
		int ile_parzystych = 0;
        int ile_niepaprzystych = 0;
        
        
        for (int j = 0; j < n; ++j) {
            if (tablica[j] %2 == 0) {
                ile_parzystych++;
            } else {
                ile_niepaprzystych++;
            }
        }
        
        System.out.println("Liczby nieparzyste: " + ile_niepaprzystych);
        System.out.println("Liczby parzyste: " + ile_parzystych);
    }
}
