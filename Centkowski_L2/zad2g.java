import java.util.Scanner;
import java.util.Random;

public class Lab2_z2g
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        
        generuj(tablica, n, -999, 999);
        System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        int prawy = 0;
        int lewy = 0;
        
        System.out.println();
        System.out.println();
        
        while (!((lewy >= 1) && (lewy <= 100))) {
            System.out.println("Podaj lewy zakres od 1 do 100: ");
            lewy = in.nextInt();
        }
        
        while (!((prawy >= 1) && (prawy <= 100))) {
            System.out.println("Podaj prawy zakres od 1 do 100: ");
            prawy = in.nextInt();
        }

        odwrocFragment(tablica, lewy, prawy);
        
        System.out.println();
        
        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }

        System.out.println();
    }

    public static void generuj (int[] tab, int n, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max - min + 1) + min;
        }
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy) {
        for (int l = (lewy - 1), p = (prawy - 1); l < p; ++l, --p) {
            int pom = tab[l];
            tab[l] = tab[p];
            tab[p] = pom;
        }
    }
}
