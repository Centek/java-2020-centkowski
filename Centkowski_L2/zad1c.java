import java.util.Scanner;
import java.util.Random;

public class Lab2_z1c
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe od 1 do 100: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        
        int lim1 = 999;
        int lim2 = -999;
        
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(lim1 - lim2 + 1) + lim2;
        }
        
	System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        System.out.println("");
        
        int max = tablica[0];
        
        for (int j = 1; j < n; ++j) {
            if (tablica[j] > max) {
                max = tablica[j];
            }
        }
        
        int ile_razy = 0;
        
        for (int j = 0; j < n; ++j) {
            if (tablica[j] == max) {
                ++ile_razy;
            }
        }
        
        System.out.println("Liczba " + max + " wystepuje " + ile_razy + " razy");
    }
}

