import java.util.Scanner;
import java.util.Random;

public class Lab2_z1b
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe od 1 do 100: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        
        int lim1 = 999;
        int lim2 = -999;
        
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(lim1 - lim2 + 1) + lim2;
        }
 
 
	System.out.println();


        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        System.out.println("");
        
        int ile_ujemnych = 0;
        int ile_zer = 0;
        int ile_dodatnich = 0;
        
        for (int j = 0; j < n; ++j) {
            if (tablica[j] < 0) {
                ile_ujemnych++;
            } else if (tablica[j] == 0) {
                ile_zer++;
            } else {
                ile_dodatnich++;
            }
        }
        
        System.out.println("Ujemne: " + ile_ujemnych);
        System.out.println("Zerowe : " + ile_zer);
        System.out.println("Dodatnie: " + ile_dodatnich);
    }
}


