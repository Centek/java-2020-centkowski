import java.util.Scanner;
import java.util.Random;

public class Lab2_z1g
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        int lim1 = 999;
        int lim2 = -999;
        
        for (int j = 0; j < n; ++j) {
            tablica[j] = r.nextInt(lim1 - lim2 + 1) + lim2;
        }
        
        System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        int prawy = 0;
        int lewy = 0;
        
        System.out.println();
        System.out.println();
        
        while (!((lewy >= 1) && (lewy <= 100))) {
            System.out.println("Podaj lewy zakres od 1 do 100: ");
            lewy = in.nextInt();
        }
        
        while (!((prawy >= 1) && (prawy <= 100))) {
            System.out.println("Podaj prawy zakres od 1 do 100: ");
            prawy = in.nextInt();
        }
        
        for (int l = (lewy - 1), p = (prawy - 1); l < p; ++l, --p) {
            int pom = tablica[l];
            tablica[l] = tablica[p];
            tablica[p] = pom;
        }
        
        System.out.println();
        
        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }

        System.out.println();
    }
}


