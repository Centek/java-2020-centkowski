import java.util.Scanner;
import java.util.Random;

public class Lab2_z2d
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = 0;
        while (!((n >= 1) && (n <= 100))) {
            System.out.println("Podaj liczbe od 1 do 100: ");
            n = in.nextInt();
        }
        
        int[] tablica = new int[n];
        Random r = new Random();
        
        generuj(tablica, n, -999, 999);
        
	System.out.println();

        for (int j = 0; j < n; ++j) {
            System.out.printf("%5d", tablica[j]);
        }
        
        System.out.println("");
        
        System.out.println("Suma liczb dodatnich wynosi: " + suma_Dodatnich(tablica) + ". Suma liczb ujemnych wynosi: " + suma_Ujemnych(tablica));
    }

    public static void generuj (int[] tab, int n, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(max - min + 1) + min;
        }
    }

    public static int suma_Dodatnich(int tab[]) {
        int suma = 0;

        for (int el : tab) {
            if (el > 0) {
                suma += el;
            }
        }
        
        return suma;
    }

    public static int suma_Ujemnych(int tab[])  {
        int suma = 0;

        for (int el : tab) {
            if (el < 0) {
                suma += el;
            }
        }
        
        return suma;
    }
}
