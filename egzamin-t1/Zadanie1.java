import java.time.LocalDate;
import java.util.Arrays;

public class ArrayUtilDemo
{
	public static void main(String[] args)
	{
        Integer[] a = {1, 2, 3, 4, 5};
        for (Integer liczba : a) {
            System.out.print(liczba + " ");
        }
        
        System.out.println("\n Sortowanie: " + ArrayUtil.isSorted(a) + "\n");

        
        
        Integer[] b = {1, 9, 3, 7, 5};
        for (Integer liczba : b) {
            System.out.print(liczba + " ");
        }
        
        
        System.out.println("\n Sortowanie: " + ArrayUtil.isSorted(b) + "\n");

        
        LocalDate[] daty_a = {LocalDate.of(2000, 2, 1), LocalDate.of(2004, 3, 10), LocalDate.of(2012, 1, 12)};
        LocalDate[] daty_b = {LocalDate.of(2018, 2, 18), LocalDate.of(2016, 4, 9), LocalDate.of(2015, 6, 10)};

        
        
        for (LocalDate data : daty_a) {
            System.out.print(data + "\t");
        }
        
        System.out.println("\n Sortowanie: " + ArrayUtil.isSorted(daty_a) + "\n");

        
        
        for (LocalDate data : daty_b) {
            System.out.print(data + "\t");
        }
        
        System.out.println("\n Sortowanie: " + ArrayUtil.isSorted(daty_b) + "\n");

        System.out.println();

        System.out.println("Miejsce cyfry 4 w tablicy a: " + ArrayUtil.binSearch(a, 4));
        System.out.println("Miejsce daty 2000-02-01 w liscie daty_a: " +  ArrayUtil.binSearch(daty_a, LocalDate.of(2002, 6, 10)));
	}
}



public class ArrayUtil<T>
{
	public static <T extends Comparable> boolean isSorted(T[] a)
    {
        if (a == null || a.length == 0) {
            return false;
        }

        for (int i = 0; i < (a.length - 1); ++i) {
            if (a[i + 1].compareTo(a[i]) < 0) {
                return false;
            }
        }        
        return true;
    }

    public static <T extends Comparable> int binSearch(T[] tab, T object) {
        int a = 0;
        int b = tab.length - 1;
        int indeks;

        while (a <= b) {
            int polowa = (a + b) / 2;
            int result = object.compareTo(tab[polowa]);
            if (result == 0) {
                return polowa;
            }
        }
        return -1;
    }
}