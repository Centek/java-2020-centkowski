import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.nio.file.Paths;

public class Zadanie3 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Podaj tylko nazwe pliku!");

            return;
        }

        
        String file = Paths.get(".").toAbsolutePath().normalize() + "\\egzamin-t1\\" + args[0];

        
        try {
            InputStream inputStream = new FileInputStream(file);

            int readedByte = inputStream.read();
            int i = 0;

            while (readedByte != -1) {
                if (i >= 64) {
                    System.out.println();

                    i = 0;
                }

                if (readedByte >= 32 && readedByte <= 126) {
                    System.out.print(readedByte + ", ");

                    i += 1;
                }

                readedByte = inputStream.read();

                i += 1;
            }

            inputStream.close();
        }

        
        catch (FileNotFoundException ex) {
            System.out.println("Plik nie istnieje!");
        }

        
        catch (IOException e) {
            System.out.println("Nie udalo sie wczytac pliku! Prawdopodobnie nie istnieje");
        }
    }
}