import java.util.Scanner;
import java.util.Stack;

public class Lab3_z1h
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj liczbe: ");
        String napis = in.next();
        
        System.out.println();
        System.out.println("Podaj znak: ");
        char znak_char = in.next().charAt(0);

        System.out.println();
        System.out.println("Podaj liczbe pozycji pomiedzy znakami: ");
        int ile = in.nextInt();

        System.out.println();

        System.out.println("Napis: " + napis);
        System.out.println("Zmodyfikowany napis: " + nice(napis, znak_char, ile) + "\n");
    }
    
    public static String nice(String str, char znak, int n)
    {
        StringBuffer nice = new StringBuffer();
        
        String[] cyfry = str.split("");
        Stack<String> stack = new Stack<String>();

        for (int k = 0; k < str.length(); k++) {
            String c = cyfry[k];
            stack.push(c);
        }

        if (str.length() >= n) {
            for (int j = 0; j < (str.length() / n); ++j) {
                for (int i = 0; i < n; ++i) {
                    nice.append(stack.pop());
                }
                nice.append(znak);
            }
        } else {
            for (int m = 0; m < str.length() - (n - 1); ++m) {
                nice.append(stack.pop());
            }
        }

        for (int l = 0; l < (str.length() %n); ++l) {
            nice.append(stack.pop());
        }

        nice.reverse();

        if (nice.charAt(0) == '\'') {
            nice.deleteCharAt(0);
        }
        
        return nice.toString();
    }
}
