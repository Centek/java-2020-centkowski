import java.math.BigDecimal;
import java.io.FileNotFoundException;
import java.math.RoundingMode;

public class Lab3_z5 {
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length != 3) {
            System.err.println("Sposob uzycia: java TestFileScanner nazwaPliku");
            System.exit(1);
        }

        BigDecimal k = new BigDecimal(args[0]);
        BigDecimal p = new BigDecimal(args[1]);
        int n = Integer.parseInt(args[2]);
        
        BigDecimal a = p.divide(new BigDecimal(100));
        BigDecimal b = a.add(new BigDecimal(1));
        BigDecimal c = b.pow(n);
        BigDecimal kwota = c.multiply(k).setScale(2, RoundingMode.HALF_UP);

        System.out.println("\n Kwota koncowa wynosi: " + kwota.toString());
    }
}
