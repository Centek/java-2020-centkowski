import java.util.Scanner;
import java.util.Stack;

public class Lab3_z1e 
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
        System.out.println("Podaj inne slowo: ");
        String drugie_slowo = in.next();
        
        int[] wynik = where(slowo, drugie_slowo);
        
        System.out.print("\n Indeksy: ");
        for (int k = 0; k < wynik.length; ++k) {
            System.out.print(wynik[k] + " ");
        }
        System.out.println();        
    }
    
    public static int[] where(String str, String subStr)
    {
        Stack<Integer> liczby = new Stack<Integer>();
        
        for (int j = 0; j < (str.length() - subStr.length() + 1); ++j) {
            
            if (subStr.charAt(0) == str.charAt(j)) {
                boolean isTheSame = true;
                for (int k = 0; k < subStr.length(); ++k) {
                    if (subStr.charAt(k) != str.charAt(k + j)) {
                        isTheSame = false;
                    }
                }
                if (isTheSame == true) {
                    liczby.push(j);
                }
            }
        }
        
        int length = liczby.size();
        int[] indeksy = new int[length];
        
        for (int i = 0; i < length; ++i) {
            indeksy[i] = liczby.pop();
        }
        
        for (int l = 0, p = (length - 1); l < p; ++l, --p) {
            int pom = indeksy[l];
            indeksy[l] = indeksy[p];
            indeksy[p] = pom;
        }
        
    return indeksy;
    }
}
