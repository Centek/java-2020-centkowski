import java.util.Scanner;

public class Lab3_z1d 
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
        System.out.println("Podaj liczbe powtorzen: ");
        int n = in.nextInt();
        
        System.out.println("Wynik: " + repeat(slowo, n));
    }
    
    public static String repeat(String str, int n)
    {
        StringBuffer strBuff = new StringBuffer();
        
        for (int j = 0; j < n; ++j) {
            strBuff.append(str);
        }
        
        return strBuff.toString();
    }
}
