import java.util.Scanner;

public class Lab3_z1f
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
        System.out.println();
        System.out.println("slowo: " + slowo);
        System.out.println("Odwrocone slowo: " + change(slowo) + "\n"); 
    }
    
    public static String change(String str)
    {
        StringBuffer changed = new StringBuffer();
        for (int j = 0; j < str.length(); ++j) {
            if (Character.isUpperCase(str.charAt(j))) {
                changed.append(str.toLowerCase().charAt(j));
            } else if (Character.isLowerCase(str.charAt(j))) {
                changed.append(str.toUpperCase().charAt(j));
            } else {
                changed.append(str.charAt(j));
            }
        }
        return changed.toString();
    }
}
