import java.util.Scanner;
import java.util.Stack;

public class Lab3_z1g
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
        System.out.println();

        System.out.println("Slowo: " + slowo);
        System.out.println("Zmodyfikowane slowo: " + nice(slowo) + "\n");
    }
    
    public static String nice(String str)
    {
        StringBuffer nice = new StringBuffer();
        
        String[] cyfry = str.split("");
        Stack<String> stack = new Stack<String>();

        for (int k = 0; k < str.length(); k++) {
            String c = cyfry[k];
            stack.push(c);
        }

        if (str.length() >= 3) {
            for (int j = 0; j < (str.length() / 3); ++j) {
                for (int i = 0; i < 3; ++i) {
                    nice.append(stack.pop());
                }
                nice.append("'");
            }
        } else {
            for (int m = 0; m < str.length() - 2; ++m) {
                nice.append(stack.pop());
            }
        }

        for (int l = 0; l < (str.length() %3); ++l) {
            nice.append(stack.pop());
        }

        nice.reverse();

        if (nice.charAt(0) == '\'') {
            nice.deleteCharAt(0);
        }
        
        return nice.toString();
    }
}
