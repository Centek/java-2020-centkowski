import java.util.Scanner;

public class Lab3_z1c 
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
        System.out.println("Wynik: " + middle(slowo));
    }
    
    public static String middle(String str)
    {
        StringBuffer strBuff = new StringBuffer();
        int length = str.length();
        
        if (length %2 == 0) {
            strBuff.append(str.charAt(length/2 - 1));
            strBuff.append(str.charAt(length/2));
        } else {
            strBuff.append(str.charAt(length/2));
        }
        
        return strBuff.toString();
    }
}
