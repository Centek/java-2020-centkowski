import java.util.Scanner;

public class Lab3_z1b 
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
        System.out.println("Podaj inne slowo: ");
        String drugie_slowo = in.next();
        
        System.out.println("\n Liczba wystapien slowa: " + countSubStr(slowo, drugie_slowo));
    }
    
    public static int countSubStr(String str, String subStr)
    {
        int ile_znakow = 0;
        
        
        for (int j = 0; j < (str.length() - subStr.length() + 1); ++j) {
            
            if (str.charAt(j) == subStr.charAt(0)) {
                boolean isTheSame = true;
                for (int k = 1; k < subStr.length(); ++k) {
                    if (subStr.charAt(k) != str.charAt(j + k)) {
                        isTheSame = false;
                    }
                }
                
                if (isTheSame == true) {
                    ++ile_znakow;
                }
            }
        }
        
        return ile_znakow;
    }
}
