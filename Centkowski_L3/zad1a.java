import java.util.Scanner;

public class Lab3_z1a 
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
		
        System.out.println("Podaj jakies slowo: ");
        String slowo = in.next();
        
		
        System.out.println("Podaj znak: ");
        char znak = in.next().charAt(0);
        
		
        System.out.println("\n Ilosc znakow w slowie: " + countChar(slowo, znak));
    }
    
    public static int countChar(String str, char c)
    {
        int ile_znakow = 0;
        
        for (int j = 0; j < str.length(); ++j) {
            if (str.charAt(j) == c) {
                ++ile_znakow;
            }
        }
        
        return ile_znakow;
    }
}
