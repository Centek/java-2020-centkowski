import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Lab3_z2
{
	public static void main(String[] args) throws FileNotFoundException {
		if (args.length != 2) {
		    System.err.println("Musisz podac dwa argumenty: ");
		    System.exit(1);
		}
		
		Scanner file = new Scanner(new File(args[0]));
		int ile_znakow = 0;
		
		while (file.hasNextLine()) {
		    Scanner str = new Scanner(file.nextLine());
		    while (str.hasNext()) {
		        String s = str.next();
		        for (int i = 0; i < s.length(); ++i) {
		            if (s.charAt(i) == args[1].charAt(0)) {
		                ++ile_znakow;
		            }
		        }
		    }
		}
		
		System.out.println("Ilosc znakow w pliku: " + ile_znakow);
	}
}
