import java.math.BigInteger;
import java.io.FileNotFoundException;

public class Lab3_z4 {
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length != 1) {
            System.err.println("Sposob uzycia: java TestFileScanner nazwaPliku");
            System.exit(1);
        }

        int n = Integer.parseInt(args[0]);
        int ilosc_pol = n * n;

        BigInteger suma = new BigInteger("0");
        BigInteger potega;
        BigInteger podstawa = new BigInteger("2");

        for (int j = 0; j < ilosc_pol; ++j) {
            potega = podstawa.pow(j);
            suma = suma.add(potega);
        }

        System.out.println("Suma wynosi: " + suma);
    }
}
