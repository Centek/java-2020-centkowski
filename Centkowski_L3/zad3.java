import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Lab3_z3
{
	public static void main(String[] args) throws FileNotFoundException {
		if (args.length != 2) {
		    System.err.println("Musisz podac dwa argumenty: ");
		    System.exit(1);
		}
		
		Scanner file = new Scanner(new File(args[0]));
		int ile_znakow = 0;
		
		while (file.hasNextLine()) {
		    Scanner str = new Scanner(file.nextLine());
		    while (str.hasNext()) {
		        String s = str.next();
		        for (int i = 0; i < (s.length() - args[1].length() + 1); ++i) {
		            if (s.charAt(i) == args[1].charAt(0)) {
		                boolean isTheSame = true;
		                for (int k = 1; k < (args[1].length()); ++k) {
		                    if (args[1].charAt(k) != s.charAt(k + i)) {
		                        isTheSame = false;
		                    }
		                }
		                
		                if (isTheSame == true) {
		                    ++ile_znakow;
		                }
		            }
		        }
		    }
		}
		
		System.out.println("Ilosc znakow w pliku: " + ile_znakow);
	}
}

