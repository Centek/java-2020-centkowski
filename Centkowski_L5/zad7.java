public class Lab5_z7
{
	public static void main(String[] args)
	{
		Car c = new Car(8, 40);
		System.out.println("Ilosc benzyny w litrach: " + c.getLitryBenzyny());
		c.zatankuj(20);
		System.out.println("Ilosc benzyny w litrach: " + c.getLitryBenzyny());
		c.drive(50);
		System.out.println("Ilosc benzyny w litrach: " + c.getLitryBenzyny());
		
		System.out.println("Ilosc przejechanych kilometrow: " + c.getKm());
		c.drive(100);
		System.out.println("Ilosc przejechanych kilometrow: " + c.getKm());
	}
}

class Car
{
	public Car(double spalanie, double litryBenzyny)
	{
		this.spalanie = spalanie;
		this.litryBenzyny = litryBenzyny;
		this.kilometry = 0;
	}

	public double getSpalanie()
	{
		return spalanie;
	}

	public double getLitryBenzyny()
	{
		return litryBenzyny;
	}

	public void drive(double przejechanaDroga)
	{
		kilometry += przejechanaDroga;
		litryBenzyny -= (przejechanaDroga * spalanie / 100);
	}

	public void zatankuj(double benzyna)
	{
		litryBenzyny += benzyna;
	}

	public double getKm()
	{
		return kilometry;
	}

	private double spalanie;
	private double litryBenzyny;
	private double kilometry;
}
