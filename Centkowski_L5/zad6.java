public class Lab5_z6 
{
	public static void main(String[] args)
	{
		Point p = new Point(1, 2);
		System.out.println(p.toString());
		p.translate(5, 10);
		System.out.println(p.toString());
		p.scale(0.5);
		System.out.println(p.toString());
	}
}

class Point 
{
	public Point(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public String toString()
	{
		return ("<" + x + ", " + y + ">");
	}

	double getX()
	{
		return x;
	}

	double getY()
	{
		return y;
	}

	void translate(double deltaX, double deltaY)
	{
		x += deltaX;
		y += deltaY;
	}

	void scale(double delta)
	{
		x *= delta;
		y *= delta;
	}

	private double x;
	private double y;
}
