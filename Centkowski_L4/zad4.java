import java.util.ArrayList;
import java.util.Arrays;

public class Lab4_z4 
{
    public static void main(String[] args) 
    {
        ArrayList<Integer> lista = new ArrayList<>(Arrays.asList(1, 5, 7, 9));
        System.out.println("\n Lista: " + lista);
        System.out.println("Odwrocona lista: " + reversed(lista));
    }
    
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> b = new ArrayList<>();
        
        for (int j = a.size() - 1; j >= 0; --j) {
            b.add(a.get(j));
        }
        
        return b;
    }
}

