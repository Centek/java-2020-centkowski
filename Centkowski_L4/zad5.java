import java.util.ArrayList;
import java.util.Arrays;

public class Lab4_z5 
{
    public static void main(String[] args) 
    {
        ArrayList<Integer> lista = new ArrayList<>(Arrays.asList(1, 5, 7, 9));
        System.out.println("\n Lista: " + lista);
        reverse(lista);
        System.out.println("Odwrocona lista: " + lista);
    }
    
    public static void reverse(ArrayList<Integer> a)
    {
        for (int i = 0, j = a.size() - 1; i < j; ++i, --j) {
            int pom = a.get(i);
            a.set(i, a.get(j));
            a.set(j, pom);
        }
    }
}


