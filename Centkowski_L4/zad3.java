import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Lab4_z3 
{
    public static void main(String[] args)
    {
        ArrayList<Integer> pierwsza = new ArrayList<>(Arrays.asList(1, 4, 9, 16));
        ArrayList<Integer> druga = new ArrayList<>(Arrays.asList(9, 7, 4, 9, 11));
        
        System.out.println("Pierwsza lista: " + pierwsza);
        System.out.println("Druga lista: " + druga);
        System.out.println("Złączona i posortowana lista pierwsza i druga: " + mergeSorted(pierwsza, druga));
    }
    
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        
        ArrayList<Integer> c = new ArrayList<>();
        
        Collections.sort(a);
        Collections.sort(b);
        
        int i_a = 0, i_b = 0;
        
        while ((i_a < a.size()) && (i_b < b.size())) {
            if (a.get(i_a) < b.get(i_b)) {
                c.add(a.get(i_a));
                ++i_a;
            } else {
                c.add(b.get(i_b));
                ++i_b;
            }
        }
        
        if (i_a < a.size()) {
            for (int i = i_a; i < a.size(); ++i) {
                c.add(a.get(i));
            }
        } else if (i_b < b.size()) {
            for (int i = i_b; i < b.size(); ++i) {
                c.add(b.get(i));
            }
        }
        
        return c;
    }
}

