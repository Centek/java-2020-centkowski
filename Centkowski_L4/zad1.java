import java.util.ArrayList;
import java.util.Arrays;

public class Lab4_z1 
{
    public static void main(String[] args)
    {
        ArrayList<Integer> pierwsza = new ArrayList<>(Arrays.asList(1, 4, 9, 16));
        ArrayList<Integer> druga = new ArrayList<>(Arrays.asList(9, 7, 4, 9, 11));
        
        System.out.println("Pierwsza lista: " + pierwsza);
        System.out.println("Druga lista: " + druga);
        System.out.println("Złączona lista pierwsza i druga: " + append(pierwsza, druga));
    }
    
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        
        for (int el : b) {
            a.add(el);
        }
        
        return a;
    }
}
