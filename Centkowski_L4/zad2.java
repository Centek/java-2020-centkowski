import java.util.ArrayList;
import java.util.Arrays;

public class Lab4_z2 
{
    public static void main(String[] args)
    {
        ArrayList<Integer> pierwsza = new ArrayList<>(Arrays.asList(1, 4, 9, 16));
        ArrayList<Integer> druga = new ArrayList<>(Arrays.asList(9, 7, 4, 9, 11));
        
        System.out.println("Pierwsza lista: " + pierwsza);
        System.out.println("Druga lista: " + druga);
        System.out.println("Złączona lista pierwsza i druga: " + merge(pierwsza, druga));
    }
    
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        
        ArrayList<Integer> c = new ArrayList<>();
        
        int krotsza = Math.min(a.size(), b.size());
        ArrayList<Integer> dluzsza_lista = new ArrayList<>();
        
        if (krotsza == a.size()) {
            dluzsza_lista = b;
        } else {
            dluzsza_lista = a;
        }
        
        for (int j = 0; j < krotsza; ++j) {
            c.add(a.get(j));
            c.add(b.get(j));
        }
        
        for (int j = krotsza; j < dluzsza_lista.size(); ++j) {
            c.add(dluzsza_lista.get(j));
        }
        
        return c;
    }
}

